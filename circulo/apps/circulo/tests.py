from django.test import TestCase
import unittest
from .models import Circulo


class CirculoTest(TestCase):

    def setUp(self):
        self.circulo = Circulo(radio=5)

    def test_area(self):
        esperado = 78.54
        obtenido = self.circulo.getArea()
        self.assertEqual(esperado, obtenido)
    
    def test_perimetro(self):
        esperado = 31.42
        obtenido = self.circulo.getPerimetro()
        self.assertEqual(esperado, obtenido)
    
    def test_str(self):
        esperado = 'Circulo con radio 5'
        obtenido = self.circulo.__str__()
        self.assertEqual(esperado, obtenido)
    
    def test_validarRadio_ok(self):
        esperado = True
        obtenido = self.circulo.validarRadio(1)
        self.assertEqual(esperado, obtenido)
    
    @unittest.expectedFailure
    def test_validarRadio_error(self):        
        self.circulo.validarRadio(0)
    
    def test_setRadio(self):
        esperado = 3
        self.circulo.setRadio(3)
        self.assertEqual(esperado, self.circulo.radio)
    
    def test_multiplica(self):
        esperado = 25
        obtenido = self.circulo * 5
        self.assertEqual(esperado, obtenido.radio)
    
    @unittest.expectedFailure
    def test_multiplica_error(self):        
        self.circulo * 0
    