from django.db import models
from math import pi

# Si se instancia la clase con radio <= 0 mostrar una excepción indicando un error amigable al
# usuario e impidiendo la instanciación.


class Circulo(models.Model):
    radio = models.FloatField('Radio')

    def validarRadio(self, radio):
        if radio <= 0:
            raise Exception('No puede instanciarse una radio menor o igual a cero')
        
        return True

    def setRadio(self, new_radio):
        self.validarRadio(new_radio)
        self.radio = new_radio

    def getArea(self):
        return round(pow(self.radio, 2) * pi, 2)
        
    
    def getPerimetro(self):
        return round(2 * pi * self.radio, 2)

    def __str__(self) -> str:
        return f"Circulo con radio {self.radio}"
    
    def __mul__(self, number):   
        self.validarRadio(number)    
        return Circulo(radio=(self.radio * number))